from setuptools import setup

setup(
    name = "requests_wasm",
    version = "0.0.1",
    author = 'Stuart Fable',
    author_email = 'stuart.fable@gmail.com',
    license = 'MIT',
    keywords = 'wasm auth requests',
    description = 'WASM authentication module for requests.',
    url = 'https://github.com/sfable/requests-wasm',
    download_url = 'https://github.com/sfable/requests-wasm/archive/master.zip',
    py_modules = ['requests_wasm'],
    install_requires = 'requests',
    zip_safe = True,
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4'
    ]
)
